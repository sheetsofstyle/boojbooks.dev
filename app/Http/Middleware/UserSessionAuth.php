<?php

namespace App\Http\Middleware;

use App\Models\UserSession;
use Closure;

class UserSessionAuth
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    protected function errorResponse()
    {
        return response()->json(['errorResponse'=>'Invalid User ID'], 401);
    }

    /**
     * Check to make sure the incoming request has a valid x-authorization, which was assigned on first load
     * @param $request
     * @param Closure $next
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function handle($request, Closure $next)
    {

        if (env('APP_ENV') === 'testing') {

            $getX = $request->get('X-Authorization', false);
            $headerX = $request->header('X-Authorization', false);
            $id = $getX !== false ? $getX : $headerX;
            $user = [
                'user'=>[
                    'test'  =>  true,
                    'id'    =>  $id
                ]
            ];
            $request->merge($user);
            return $next($request);
        }

        $userID = $request->header('X-Authorization');

        if(empty($userID)){
            return $this->errorResponse();
        }
        $userIDLookup = UserSession::where('uuid','=',$userID)->first();
        if(empty($userIDLookup)){
            return $this->errorResponse();
        }

        if(isset($userIDLookup->id)){
            $request->merge(['user' => $userIDLookup]);
        }

        return $next($request);
    }

}
