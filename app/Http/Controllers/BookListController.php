<?php
namespace App\Http\Controllers;
use App\Http\Requests\BookListRequest;
use App\Models\Book;
use App\Models\BookList;
use Illuminate\Http\Request;

class BookListController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $user = (object) $request->get('user');
        $booklist = BookList::with('book')
            ->where('user_id','=',$user->id)
            ->orderBy('order','ASC')
            ->get();

        return response()->json($booklist);

    }

    /**
     * @param BookListRequest $request
     * @param BookList $bookList
     * @param Book $book
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(BookListRequest $request, BookList $bookList, Book $book)
    {

        $user = (object) $request->get('user');
        $author_key = $request->get('author_key');

        $newBook = $book->firstOrCreate([
            'author_key'=> $author_key,
            'title'=> $request->get('title'),
            'cover_edition_key'=> $request->get('cover_edition_key', null),
            'author_name'=> $request->get('author_name', null),
            'publish_date'=> $request->get('publish_date', null),
        ]);

        $newItem = $bookList->firstOrCreate([
            'user_id'=> $user->id,
            'author_key'=> $author_key,
        ]);
        $newItem->order = $newItem->id;
        $newItem->save();
        $newItem->book = $newBook;

        return response()->json($newItem);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param BookList $bookList
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, BookList $bookList)
    {
        $user = (object) $request->get('user');
        $order = $request->get('order');
        foreach($order as $key => $newOrder){
            $item = $bookList->where('id','=',$newOrder['id'])->where('user_id','=',$user->id)->update([
                'order'=> ($key + 1)
            ]);
        }

        $booklist = BookList::with('book')
            ->where('user_id','=',$user->id)
            ->orderBy('order','ASC')
            ->get();

        return response()->json($booklist);
    }

    /**
     * @param Request $request
     * @param BookList $bookList
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, BookList $bookList, $id)
    {
        $user = (object) $request->get('user');
        $destroyed = $bookList->where('user_id','=',$user->id)->where('id','=',$id)->first();
        if(empty($destroyed)){
            return response()->json(['message'=>'Book Not Found'], 404);
        }
        $destroyed->delete();
        return response()->json($destroyed, 200);
    }
}
