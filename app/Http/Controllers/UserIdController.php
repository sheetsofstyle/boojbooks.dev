<?php
namespace App\Http\Controllers;
use App\Models\UserSession;
use Illuminate\Support\Str;

class UserIdController extends Controller
{

    /**
     * Generate a random UUID to assign the user for the first time, if they don't have a cookie
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getUserID()
    {
        $userSession = UserSession::create([
            'uuid'=> Str::uuid()
        ]);
        return response()->json(['userID'=>$userSession->uuid]);
    }
}
