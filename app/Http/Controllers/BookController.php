<?php
namespace App\Http\Controllers;
use App\Models\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * @param Request $request
     * @param $author_key
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function book(Request $request, $author_key)
    {
        $user = $request->get('user');
        $url = getenv('OPEN_LIBRARY_ENDPOINT') . 'search.json?q=' . $author_key . '&limit=1&mode=everything';
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url);
        if($response->getStatusCode() !== 200){
            return response(['message'=>'Not Found'], 404);
        } else {
            $contents = json_decode($response->getBody()->getContents());
            if(isset($contents->docs[0])){
                $book = Book::hydrateBook($contents->docs[0],$user->id);
                if(isset($book->booklist) && $book->booklist !== false){
                    $booklist = $book->booklist;
                    unset($book->booklist);
                    $booklist->book = $book;
                    return response([
                        'book'  =>  $booklist
                    ]);
                } else {
                    unset($book->booklist);
                    return response([
                        'book'  =>  ['book'=>$book]
                    ]);
                }
            } else {
                return response(['message'=>'Not Found'], 404);
            }
        }
    }
}
