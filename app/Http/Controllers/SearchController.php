<?php
namespace App\Http\Controllers;
use App\Models\Book;
use App\Models\Featured;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    protected $client;
    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function search(Request $request)
    {

        $user = $request->get('user');
        $request = $request->all();
        unset($request['user']);

        $queryString = http_build_query($request);
        $url = getenv('OPEN_LIBRARY_ENDPOINT') . 'search.json?' . $queryString;
        $search = $this->requestData($url);
        if(isset($search->response)){
            $searchResults = [];
            foreach($search->response->docs as $docKey => $doc){
                $hydratedBook = Book::hydrateBook($doc, $user->id);
                if(isset($hydratedBook->booklist)){
                    $searchResults[$docKey] = $hydratedBook->booklist;
                    unset($hydratedBook->booklist);
                    $searchResults[$docKey]['book'] = $hydratedBook;
                } else {
                    $searchResults[$docKey] = $hydratedBook;
                }
            }
            return response()->json([
                'numFound'  =>  $search->response->numFound,
                'num_found' =>  $search->response->num_found,
                'start'     =>  $search->response->start,
                'docs'      =>  $searchResults
            ]);
        } else {
            return response($search->error, $search->statusCode);
        }

    }

    /**
     * @param $url
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function requestData($url)
    {
        $response = $this->client->request('GET', $url);
        if($response->getStatusCode() === 200){
            return (object) [
                'response' => json_decode($response->getBody()->getContents())
            ];
        } else {
            return (object) [
                'error'=>'Error',
                'statusCode'=>$response->getStatusCode()
            ];
        }
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function featured()
    {
        $featured = Featured::with('book')->inRandomOrder()->take(16)->get();
        if($featured->count() >= 16){
            return response(['featured'=>$featured]);
        }

        $page = 1;
        $limit = 100;
        $query = 'Real Estate Company';
        $url = getenv('OPEN_LIBRARY_ENDPOINT') . 'search.json?page='.$page.'&limit='.$limit.'&q='.urlencode($query);
        $data = $this->requestData($url);
        $featured = [];
        if(isset($data->response->docs)){
            foreach($data->response->docs as $book){
                $hydratedBook = Book::hydrateBook($book, false);
                if(isset($hydratedBook->cover_edition_key) && $hydratedBook->cover_edition_key !== false){
                    $featured[] = $hydratedBook;
                }
            }
        }

        /**
         * Create entries, so we don't have to do another api call to get the featured items
         */
        foreach($featured as $book){
            Featured::firstOrCreate([
                'author_key'    =>  $book->author_key
            ]);
            Book::firstOrCreate([
                'author_key'        =>  $book->author_key,
                'author_name'       =>  $book->author_name,
                'cover_edition_key' =>  $book->cover_edition_key,
                'publish_date'      =>  $book->publish_date,
                'title'             =>  $book->title,
            ]);
        }

        return response(['featured'=>Featured::with('book')->inRandomOrder()->take(16)->get()]);
    }
}
