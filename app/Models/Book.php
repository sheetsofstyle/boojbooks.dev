<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use SoftDeletes;
    protected $table = 'books';
    protected $guarded = [];
    protected $hidden = ['deleted_at', 'updated_at'];


    /**
     * Take a Guzzle Response for a book and transform it
     * @param $response
     * @param $user_id
     * @return object
     */
    public static function hydrateBook($response, $user_id)
    {
        $author_key = (isset($response->key)) ? str_replace('/works/','',$response->key) : false;
        $booklist = BookList::where('author_key','=',$author_key)->where('user_id','=',$user_id)->first();
        return (object) [
            'author_key'        =>  $author_key,
            'publish_date'      =>  isset($response->publish_date[0]) ? $response->publish_date[0] : false,
            'title'             =>  isset($response->title) ? $response->title : false,
            'cover_edition_key' =>  isset($response->cover_edition_key) ? $response->cover_edition_key : false,
            'author_name'       =>  isset($response->author_name[0]) ? $response->author_name[0] : false,
            'booklist'          =>  isset($booklist->id) ? $booklist : false,
        ];
    }


}
