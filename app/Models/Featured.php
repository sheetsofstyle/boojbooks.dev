<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Featured extends Model
{
    use SoftDeletes;
    protected $table = 'featured';
    protected $guarded = [];
    protected $visible = ['book','author_key'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function book(){
        return $this->hasOne(Book::class,'author_key','author_key');
    }
}
