<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookList extends Model
{
    use SoftDeletes;
    protected $table = 'book_lists';
    protected $guarded = [];
    protected $hidden = ['deleted_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function book()
    {
        return $this->hasOne(Book::class,'author_key','author_key');
    }

}
