<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('author_key', 32);
            $table->string('title', 256);
            $table->string('cover_edition_key', 32)->nullable();
            $table->string('author_name', 128)->nullable();
            $table->string('publish_date', 64)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index(['author_key']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
