<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\BookList;
use Faker\Generator as Faker;

$factory->define(BookList::class, function (Faker $faker) {
    static $number = 1;
    return [
        'user_id'       => $number++,
        'author_key'    => $faker->randomDigit,
        'created_at'    => now(),
        'updated_at'    => now(),
        'order'         => $number++,
        'deleted_at'    => NULL,
    ];
});
