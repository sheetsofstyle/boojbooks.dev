<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use App\Models\UserSession;
use Faker\Generator as Faker;

$factory->define(UserSession::class, function (Faker $faker) {
    return [
        'id'            => 1,
        'uuid'          => $faker->uuid,
        'created_at'    => now(),
        'updated_at'    => now(),
        'deleted_at'    => NULL,
    ];
});
