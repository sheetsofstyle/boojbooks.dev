<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Featured;
use Faker\Generator as Faker;

$factory->define(Featured::class, function (Faker $faker) {
    return [
        'author_key'    => $faker->randomDigit,
        'created_at'    => now(),
        'updated_at'    => now(),
        'deleted_at'    => NULL,
    ];
});
