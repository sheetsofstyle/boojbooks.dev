<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Book;
use Faker\Generator as Faker;

$factory->define(Book::class, function (Faker $faker) {
    return [
        'author_key'        => $faker->randomDigit,
        'cover_edition_key' => $faker->name,
        'title'             => $faker->name,
        'author_name'       => $faker->name,
        'publish_date'      => now(),
        'created_at'        => now(),
        'updated_at'        => now(),
        'deleted_at'        => NULL,
    ];
});
