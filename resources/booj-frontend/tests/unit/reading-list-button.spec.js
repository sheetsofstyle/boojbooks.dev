import { shallowMount } from '@vue/test-utils'
import readingListButton from '@/components/reading-list-button.vue'
describe('readingListButton', () => {
  it('displays a button for removing an item from list', () => {
    const item = {"id":1,"user_id":1,"author_key":"123","order":1,"created_at":new Date(),"book":{"id":1,"author_key":"123","title":"Example","cover_edition_key":"123123","author_name":"Mr. Tester","publish_date":new Date(),"created_at":new Date()}};
    const wrapper = shallowMount(readingListButton, {
      propsData: { item },
      stubs: ['router-link']
    });
    expect(wrapper.text()).toBe('Remove From Reading List')

  })
})

describe('readingListButton', () => {
    it('displays a button for adding an item from list', () => {
        const item = {"book":{"id":1,"author_key":"123","title":"Example","cover_edition_key":"123123","author_name":"Mr. Tester","publish_date":new Date(),"created_at":new Date()}};
        const wrapper = shallowMount(readingListButton, {
            propsData: { item },
            stubs: ['router-link']
        });
        expect(wrapper.text()).toBe('Add To Reading List')
    })
})
