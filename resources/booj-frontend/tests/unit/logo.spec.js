import {shallowMount} from '@vue/test-utils'
import logo from '@/components/logo.vue'

describe('logo', () => {
    it('renders the logo component', () => {

        const wrapper = shallowMount(logo, { stubs: ['router-link'] });
        expect(wrapper.classes()).toContain('logo')

    })
})
