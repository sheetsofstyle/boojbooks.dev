import Vuex from "vuex"
import {mount, createLocalVue} from '@vue/test-utils'
import readingList from '@/components/reading-list.vue'
const localVue = createLocalVue()
localVue.use(Vuex)

const store = new Vuex.Store({
    state: {
        readingList: [
            {"id":1,"user_id":1,"author_key":"123","order":1,"created_at":new Date(),"book":{"id":1,"author_key":"123","title":"Example","cover_edition_key":"123123","author_name":"Mr. Tester","publish_date":new Date(),"created_at":new Date()}},
            {"id":2,"user_id":1,"author_key":"werwer","order":2,"created_at":new Date(),"book":{"id":2,"author_key":"123123","title":"Example 2","cover_edition_key":"123123","author_name":"Mr. Tester","publish_date":new Date(),"created_at":new Date()}}
        ]
    },
    getters:{
        readingList: state => state.readingList,
    },
});

describe("ComponentWithVuex", () => {
    it("renders a button with a count", () => {
        const wrapper = mount(readingList, {
            store,
            localVue,
            stubs: ['router-link']
        })
        expect(wrapper.find(".reading-count").text()).toBe("2")
    })
})
