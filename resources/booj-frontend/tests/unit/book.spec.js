import {shallowMount} from '@vue/test-utils'
import book from '@/components/book.vue'

describe('book', () => {
    it('renders a book example with prop', () => {
        const item = {book: {title: 'example title'}}
        const wrapper = shallowMount(book, {
            propsData: {item},
            stubs: ['router-link']
        });
        expect(wrapper.text()).toMatch(item.book.title)
    })
})
