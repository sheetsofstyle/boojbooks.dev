import {shallowMount} from '@vue/test-utils'
import searchForm from '@/components/search-form.vue'

describe('logo', () => {
    it('renders the search-form component', () => {
        const wrapper = shallowMount(searchForm, { stubs: ['router-link'] });
        expect(wrapper.classes()).toContain('search-form')
    })
})
