import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
      featured: false,
      userID: false,
      searchResults: false,
      searchTerm: '',
      page:1,
      readingList:[],
      book: false,
  },
  mutations: {
      featured(state, object){
          state.featured = object;
      },
      userID(state, value){
          state.userID = value;
      },
      searchResults(state, object){
          state.searchResults = object;
      },
      removeFromSearchResults(state, object){

          if(state.searchResults && state.searchResults.docs && state.searchResults.docs.length > 0){
              state.searchResults.docs = [...state.searchResults.docs].map(item => {
                  if(item.author_key === object.author_key){
                      item = {
                          book: item.book
                      }
                  }
                  return item;

              }).filter(item => { return item;});
          }
      },
      updateSearchResults(state, object){
          if(state.searchResults && state.searchResults.docs && state.searchResults.docs.length > 0){
              state.searchResults.docs = [...state.searchResults.docs].map(item => {
                  if(item.book.author_key === object.book.author_key){
                      item = object;
                  }
                  return item;
              });
          }
      },
      searchTerm(state, value){
          state.searchTerm = value;
      },
      page(state, value){
          state.page = value;
      },
      readingList(state, object){
          state.readingList = object;
      },
      addToReadingList(state, object){
          state.readingList.push(object);
          if(state.book !== false && object.booklist){
              state.book.booklist = object.booklist;
          }
      },
      removeFromReadingList(state, object){
          state.readingList = [...state.readingList].map(item => {
              if(item.id === object.id){
                  item = false;
                  return item;
              } else {
                  return item;
              }
          }).filter(item => { return item;});
      },
      book(state,object){
          state.book = object;
      },
      removeFromBook(state, object){
          if(state.book && state.book !== false){
              state.book = object;
          }
      },
      addToBook(state, object){
          if(state.book && state.book !== false){
              state.book = object;
          }
      }
  },
    getters:{
        featured: state => state.featured,
        userID: state => state.userID,
        searchResults: state => state.searchResults,
        searchTerm: state => state.searchTerm,
        page: state => state.page,
        readingList: state => state.readingList,
        book: state => state.book,
    },
})
