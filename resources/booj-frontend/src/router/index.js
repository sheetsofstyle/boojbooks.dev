import Vue from 'vue'
import VueRouter from 'vue-router'
import index from '../views/index.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'index',
        component: index
    },
    {
        path: '/reading-list',
        name: 'reading-list',
        // route level code-splitting
        // this generates a separate chunk (works.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "works" */ '../views/reading-list.vue')
    },
    {
        path: '/works/:id',
        name: 'work id',
        // route level code-splitting
        // this generates a separate chunk (works.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "works" */ '../views/work.vue')
    },
    {
        path: '/search/:query',
        name: 'search term',
        // route level code-splitting
        // this generates a separate chunk (search.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "search" */ '../views/search.vue')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: '/', //process.env.BASE_URL
    routes
})

export default router
