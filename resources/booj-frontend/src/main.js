import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import axios from 'axios'
import VueAxios from 'vue-axios'

import VueCookies from 'vue-cookies'
Vue.use(VueCookies);
Vue.$cookies.config('7d')
Vue.use(VueAxios, axios);




/**
 * Handle axios calls a little better, so you don't have to access response.data everytime
 * Reject errors in a promise for later evaluation
 */

let userID = Vue.$cookies.get('userID');
if(userID){
    axios.defaults.headers.common['x-authorization'] = userID;
}

axios.interceptors.response.use( response => {
    return response.data;
}, error => {
    /**
     * Check for invalid Cookies, then remove and refresh if needed
     */
    if(error.response.status === 401){
        let cookie = Vue.$cookies.get('userID');
        if(cookie){
            Vue.$cookies.remove('userID');
            window.location = ''
            return false;
        }
    }
    return Promise.reject(error);
});

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
