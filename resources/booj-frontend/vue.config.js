const path = require("path");
module.exports = {
    outputDir: path.resolve(__dirname, "../../public/booj-books"),
    assetsDir: "../../public/booj-books",
    publicPath: process.env.NODE_ENV === 'production' ? '/booj-books' : '/',
    configureWebpack: config => {
        config.output.filename = '[name].js'
        config.output.chunkFilename = '[name].js'
    },
    css: {
        loaderOptions: {
            sass: {
                prependData: `@import "@/assets/sass/__Index.scss";`
            }
        },
        extract: {
            filename: '[name].css',
        },
    },
}
