<!DOCTYPE html>
<html lang=en>
<head>
    <meta charset=utf-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta name=viewport content="width=device-width,initial-scale=1">
    <title>Booj Books</title>
    <meta name="description" content="Booj Books - A list of books you'd like to read.">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href=/booj-books/about.js rel=prefetch>
    <link href=/booj-books/app.css rel=preload as=style>
    <link href=/booj-books/app.js rel=preload as=script>
    <link href=/booj-books/chunk-vendors.js rel=preload as=script>
    <link href=/booj-books/app.css rel=stylesheet>
</head>
<body>
<noscript><strong>We're sorry but booj-frontend doesn't work properly without JavaScript enabled. Please enable it to
        continue.</strong></noscript>
<div id=app></div>
<script src=/booj-books/chunk-vendors.js></script>
<script src=/booj-books/app.js></script>
</body>
</html>
