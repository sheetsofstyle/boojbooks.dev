# Booj Books

Compose a site using the Laravel or Vue framework that allows the user to create a list of books they would like to read. Users should be able to perform the following actions:

* Connect to a publicly available API
* Create Postman collection and Vue app OR Laravel App
* Add or remove items from the list
* Change the order of the items in the list
* Sort the list of items
* Display a detail page with at least 3 points of data to display
* Include unit tests
* Deploy it on the cloud - be prepared to describe your process on deployment



## Localized Setup instructions
* Unzip files to the desired location
* Copy the env.example file and update the DB_ information
    * cp env.example .env
* Run: composer install
* Run migrations: php artisan migrate
* Create an APP_KEY: php artisan key:generate
* Copy the .env.example file and update the VUE_APP_API_ENDPOINT to your local dev url
    * cp /resources/booj-frontend/env.example /resources/booj-frontend/.env.local
* Install node depenencies /resources/booj-frontend/ npm install
* Build the vue app: /resources/booj-frontend/ npm run build
* Enjoy 
