<?php

namespace Tests\Feature;

use App\Models\BookList;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DatabaseTest extends TestCase
{
    use DatabaseMigrations;
    use RefreshDatabase;
    /**
     * Make sure that the database is connected
     *
     * @return void
     */
    public function testDatabase()
    {
        $bookList = factory(BookList::class, 10)->create();
        $this->assertDatabaseHas('book_lists', [
            'id' => 5,
        ]);
    }
}
