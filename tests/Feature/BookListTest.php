<?php

namespace Tests\Feature;

use App\Models\BookList;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class BookListTest extends TestCase
{

    use DatabaseMigrations;
    use RefreshDatabase;

    /**
     * Make sure that entries can be added to the database for BookList
     *
     * @return void
     */
    public function testInsertBookList()
    {
        $bookList = factory(BookList::class)->create();
        $this->assertDatabaseHas('book_lists', [
            'id' => 1,
        ]);
        $bookList->delete();
        $this->assertSoftDeleted($bookList);

    }
}
