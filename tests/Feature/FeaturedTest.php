<?php

namespace Tests\Feature;

use App\Models\BookList;
use App\Models\Featured;
use App\Models\UserSession;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class FeaturedTest extends TestCase
{

    use DatabaseMigrations;
    use RefreshDatabase;

    /**
     * Make sure that entries can be added to the database for BookList
     *
     * @return void
     */
    public function testInsertFeatured()
    {
        $bookList = factory(Featured::class)->create();
        $this->assertDatabaseHas('featured', [
            'id' => 1,
        ]);
        $bookList->delete();
        $this->assertSoftDeleted($bookList);

    }

    /**
     * Get a list of books from the featured table, it should get 16 entries
     */
    public function testFetFeatured()
    {
        $userSession = factory(UserSession::class)->create();
        factory(Featured::class,22)->create();
        $response = $this->json('GET', 'api/featured', ['X-Authorization'=>$userSession->id]);
        $response->assertStatus(200);
        $response->assertJsonCount(16,'featured');
    }
}
