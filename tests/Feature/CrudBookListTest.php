<?php
namespace Tests\Feature;

use App\Models\Book;
use App\Models\BookList;
use App\Models\UserSession;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Str;
use Tests\TestCase;

class CrudBookListTest extends TestCase
{
    use DatabaseMigrations;
    use RefreshDatabase;

    /**
     * Fail a post
     *
     * @return void
     */
    public function testFailed()
    {
        $userSession = factory(UserSession::class)->create();
        $response = $this->json('POST', 'api/booklist', [],['X-Authorization'=>$userSession->id]);
        $response->assertStatus(422);

    }

    /**
     * Create an item
     */
    public function testCreate()
    {
        $userSession = factory(UserSession::class)->create([
            'id'=> rand(100, 200),
            'uuid'=> Str::uuid(),
        ]);
        $response = $this->json('POST', 'api/booklist', [
            'author_key'=> 9999,
            'title'=>'example book',
        ],['X-Authorization'=>$userSession->id]);

        $response->assertStatus(200);
    }

    /**
     * Get a list of books
     */
    public function testRead()
    {
        $userSession = factory(UserSession::class)->create();
        $bookList = factory(BookList::class,10)->create();
        $response = $this->json('GET', 'api/booklist', ['X-Authorization'=>$userSession->id]);
        $response->assertStatus(200);
    }

    /**
     * Remove and item
     */
    public function testDestroy()
    {
        $book = factory(Book::class)->create();
        $bookList = factory(BookList::class)->create();
        $response = $this->json('DELETE', 'api/booklist/' . $bookList->id, ['X-Authorization'=>$bookList->user_id]);
        $response->assertStatus(200);
    }

    /**
     * Create a list of saved books
     * Shuffle the order and change the order
     */
    public function testUpdate()
    {
        $booklist = factory(BookList::class, 10)->create();
        $bookArray = $booklist->toArray();
        shuffle($bookArray);
        foreach($bookArray as $key => $value){
            $bookArray[$key]['order'] = ($key + 1);
        }

        $userSession = factory(UserSession::class)->create([
            'id'=> rand(100, 200),
            'uuid'=> Str::uuid(),
        ]);
        $response = $this->json('PATCH', 'api/booklist', ['order'=>$bookArray],['X-Authorization'=>$userSession->id]);
        $response->assertStatus(200);

    }
}
