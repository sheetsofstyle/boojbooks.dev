<?php

namespace Tests\Feature;

use App\Models\UserSession;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserSessionTest extends TestCase
{
    use DatabaseMigrations;
    use RefreshDatabase;


    /**
     * Make sure that UserSession can create and delete entries
     *
     * @return void
     */
    public function testUserSession()
    {
        $userSession = factory(UserSession::class)->create();
        $this->assertDatabaseCount('user_sessions', 1);
        $userSession->delete();
        $this->assertSoftDeleted($userSession);
    }
}
