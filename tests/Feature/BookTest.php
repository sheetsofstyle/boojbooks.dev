<?php

namespace Tests\Feature;

use App\Models\Book;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BookTest extends TestCase
{
    use DatabaseMigrations;
    use RefreshDatabase;

    /**
     * Make sure that entries can be added to the database for BookList
     *
     * @return void
     */
    public function testInsertBook()
    {
        $bookList = factory(Book::class)->create();
        $this->assertDatabaseHas('books', [
            'id' => 1,
        ]);
        $bookList->delete();
        $this->assertSoftDeleted($bookList);

    }
}
