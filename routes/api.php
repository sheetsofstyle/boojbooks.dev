<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/get-user-id','UserIdController@getUserID');


Route::group(['middleware' => ['uuid.auth']], function () {
    /**
     * Search the openlibrary api
     */
    Route::post('/search', 'SearchController@search');
    Route::get('/featured', 'SearchController@featured');

    /**
     * Get a book's details from it's author_key
     */
    Route::get('/book/{author_key}', 'BookController@book');

    /**
     * GET, POST, DETELE, PATCH routes for booklist
     */
    Route::get('/booklist/', 'BookListController@index');
    Route::post('/booklist/', 'BookListController@store');
    Route::delete('/booklist/{id}', 'BookListController@destroy');
    Route::patch('/booklist/', 'BookListController@update');
});


